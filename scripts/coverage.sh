#!/bin/env sh

COVERAGE_DIR="${COVERAGE_DIR:-coverage}"

PACKAGE_LIST=$(go list ./...)

mkdir -p ${COVERAGE_DIR}

for package in ${PACKAGE_LIST}; do
	go test -covermode=count -coverprofile "${COVERAGE_DIR}/${package##*/}.cov" "$package" ;
done

echo 'mode: count' > "${COVERAGE_DIR}"/coverage.cov ;
tail -q -n +2 "${COVERAGE_DIR}"/*.cov >> "${COVERAGE_DIR}"/coverage.cov ;

go tool cover -func="${COVERAGE_DIR}"/coverage.cov ;

for package in ${COVERAGE_DIR}/*; do
	if [ "$package" != "${COVERAGE_DIR}/coverage.cov" ]; then 
		rm $package
	fi
done

if [ "$1" == "html" ]; then
    go tool cover -html="${COVERAGE_DIR}"/coverage.cov -o ${COVERAGE_DIR}/coverage.html ;
fi
