PROJECT_HOME=$(pwd)

GIT_COMMIT=$(git rev-list -1 HEAD)
PACKAGE="gitlab.com/redstu-project/redstu-client"
BUILDER=$PACKAGE/internal/config.user=$(id -u -n)
DATE=$PACKAGE/internal/config.date=$(date)
COMMIT=$PACKAGE/internal/config.commit=$GIT_COMMIT
BRANCH=$PACKAGE/internal/config.branch=$(git rev-parse --abbrev-ref HEAD)


usage() {
	echo "Invalid build target"
	exit 1
}

build_linux() {
	GOOS=linux CGO_ENABLED=0 go build -ldflags="-s -w -X '$BUILDER' -X '$DATE' -X '$COMMIT' -X '$BRANCH'" -o bin/linux/amd64/redstu cmd/redstu/main.go
}

build_windows() {
	GOOS=windows CGO_ENABLED=0 go build -ldflags="-s -w -X '$BUILDER' -X '$DATE' -X '$COMMIT' -X '$BRANCH'" -o bin/windows/amd64/redstu.exe cmd/redstu/main.go
}

build_macos() {
	GOOS=darwin CGO_ENABLED=0 go build -ldflags="-s -w -X '$BUILDER' -X '$DATE' -X '$COMMIT' -X '$BRANCH'" -o bin/macos/amd64/redstu cmd/redstu/main.go
}

while [ "$1" != "" ]; do
	case $1 in
	    linux)
			echo "Building Linux binary"
			build_linux
			;;
	    windows)
			echo "Building Windows binary"
	        build_windows
			;;
	    macos)
	        echo "Building macOS binary"
			build_macos
			;;
	    *)
	        usage
			;;
	esac
	shift
done
