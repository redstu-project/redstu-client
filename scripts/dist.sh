#!/bin/env sh

PROJECT_NAME=$(pwd)

function usage() {
    echo "Error"
    exit 1
}

dist_linux() {
	test -d dist || mkdir dist
	tar cvzf dist/redstu-linux-64bit.tar.gz -C bin/linux/amd64/ .
	
	tar cvjf dist/redstu-linux-64bit.tar.bz2 -C bin/linux/amd64/ .
	zip -j dist/redstu-linux-64bit.zip bin/linux/amd64/* 
}

dist_windows() {
	test -d dist || mkdir dist
	zip -j dist/redstu-windows-64bit.zip bin/windows/amd64/*
}

dist_macos() {
	test -d dist || mkdir dist
	tar cvzf dist/redstu-macos-64bit.tar.gz -C bin/macos/amd64/ .
	tar cvjf dist/redstu-macos-64bit.tar.bz2 -C bin/macos/amd64/ .
	zip -j dist/redstu-macos-64bit.zip bin/macos/amd64/* 
}

while [ "$1" != "" ]; do
	case $1 in
		linux)
		    dist_linux
		    ;;
		macos)
		    dist_macos
		    ;;
		windows)
		    dist_windows
		    ;;
	    *)
            usage
            ;;
	esac
	shift
done
