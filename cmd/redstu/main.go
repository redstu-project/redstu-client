package main

import (
	"gitlab.com/redstu-project/redstu-client/internal/commands/redstu"
)

func main() {
	redstu.Execute()
}
