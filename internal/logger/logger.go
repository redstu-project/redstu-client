package logger

import (
	"go.uber.org/zap"
	_ "go.uber.org/zap/zapcore"
	"log"
)

var DefaultLogger *zap.Logger

func init() {
	if logger, err := zap.NewProduction(); err == nil {
		DefaultLogger = logger
	} else {
		log.Fatal("Can not open logger")
	}
}
