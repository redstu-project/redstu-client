package config

import "encoding/xml"

type versionInfo struct {
	XMLName   xml.Name `json:"-" yaml:"-" xml:"version-info"`
	Name      string   `json:"name" yaml:"name" xml:"name"`
	Copyright string   `json:"copyright" yaml:"copyright" xml:"copyright"`
	Version   string   `json:"version" yaml:"version" xml:"version"`
	Commit    string   `json:"commit" yaml:"commit" xml:"commit"`
	Date      string   `json:"date" yaml:"date" xml:"date"`
	BuiltBy   string   `json:"builtBy" yaml:"builtBy" xml:"built-by"`
	Branch    string   `json:"branch" yaml:"branch" xml:"branch"`
}

var date string
var user string
var commit string
var version string
var branch string

var VersionInfo versionInfo

func init() {
	if date == "" {
		date = "unknown"
	}

	if commit == "" {
		commit = "unknown"
	}

	if version == "" {
		version = "dev"
	}

	VersionInfo = versionInfo{
		Name:      "Redstu Client",
		Copyright: `Copyright (C) 2021 Redstu Project`,
		Version:   version,
		Commit:    commit,
		Date:      date,
		BuiltBy:   user,
		Branch:    branch,
	}
}
