package version

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/redstu-project/redstu-client/internal/config"
	"gopkg.in/yaml.v2"
	"os"
)

var (
	Command = &cobra.Command{
		Use:   "version",
		Short: "Get version information",
		Long:  "Get version information",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
			if config.Params.Version.Text {
				config.Params.Version.Format = "text"
			}

			if config.Params.Version.JSON {
				config.Params.Version.Format = "json"
			}

			if config.Params.Version.YAML {
				config.Params.Version.Format = "yaml"
			}

			if config.Params.Version.XML {
				config.Params.Version.Format = "xml"
			}

			if config.Params.Version.Format == "text" {
				fmt.Printf("%s\n", config.VersionInfo.Name)
				fmt.Printf("%s\n", config.VersionInfo.Copyright)
				fmt.Printf(" Version: %s\n", config.VersionInfo.Version)
				fmt.Printf(" Git commit: %s\n", config.VersionInfo.Commit)
				fmt.Printf(" Built at: %s\n", config.VersionInfo.Date)
				fmt.Printf(" Built by: %s\n", config.VersionInfo.BuiltBy)

			} else if config.Params.Version.Format == "json" {
				if versionInfo, err := json.MarshalIndent(config.VersionInfo, "", "  "); err != nil {
					fmt.Println("Unknown error")
					os.Exit(1)
				} else {
					fmt.Println(string(versionInfo))
				}
			} else if config.Params.Version.Format == "yaml" {
				if versionInfo, err := yaml.Marshal(config.VersionInfo); err != nil {
					fmt.Println("Unknown error")
					os.Exit(1)
				} else {
					fmt.Println(string(versionInfo))
				}
			} else if config.Params.Version.Format == "xml" {
				if versionInfo, err := xml.Marshal(config.VersionInfo); err != nil {
					fmt.Println("Unknown error")
					os.Exit(1)
				} else {
					fmt.Println(string(versionInfo))
				}
			} else {
				fmt.Println("Unknown output format")
				os.Exit(1)
			}
		},
	}
)

func init() {
	Command.Flags().StringVarP(&config.Params.Version.Format, "format", "f", config.Params.Version.Format, "Output format")
	Command.Flags().BoolVar(&config.Params.Version.JSON, "json", config.Params.Version.JSON, "Set output format to json")
	Command.Flags().BoolVar(&config.Params.Version.YAML, "yaml", config.Params.Version.YAML, "Set output format to yaml")
	Command.Flags().BoolVar(&config.Params.Version.Text, "text", config.Params.Version.Text, "Set output format to text")
	Command.Flags().BoolVar(&config.Params.Version.XML, "xml", config.Params.Version.XML, "Set output format to xml")
}
