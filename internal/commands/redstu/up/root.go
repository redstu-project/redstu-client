package up

import (
	"crypto/sha1"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/redstu-project/redstu-client/internal/config"
	"gitlab.com/redstu-project/redstu-client/pkg/redstu"
	"gitlab.com/redstu-project/redstu-client/pkg/redstu/docker"
	"gitlab.com/redstu-project/redstu-client/pkg/redstu/project"
	"io/ioutil"
	"os"
)

var (
	Command = &cobra.Command{
		Use:   "up",
		Short: "Start an engine process",
		Long:  "Start an engine process",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
			cwd, err := os.Getwd()
			if err != nil {
				panic(err)
			}

			manifestBlob, err := ioutil.ReadFile(fmt.Sprintf("%s/redstu.yml", cwd))
			if err != nil {
				panic(err)
			}

			manifest, err := project.LoadManifest(manifestBlob)
			if err != nil {
				panic(err)
			}

			repository := viper.GetString(toProfile(redstu.ConfigurationDefaultProfile, redstu.ConfigurationRepository))
			tag := viper.GetString(toProfile(redstu.ConfigurationDefaultProfile, redstu.ConfigurationImageTag))

			projectPath := sha1.Sum([]byte(cwd))
			if id, err := docker.RunContainer(fmt.Sprintf("%s-%x", manifest.Project, projectPath[:]), fmt.Sprintf("%s:%s", repository, tag)); err != nil {
				fmt.Println(err)
			} else {
				fmt.Println(id)
			}
		},
	}
)

func init() {
	Command.Flags().StringVarP(&config.Params.Up.Address, "address", "a", config.Params.Up.Address, "Address to listen to")
	Command.Flags().Uint16VarP(&config.Params.Up.Port, "port", "d", config.Params.Up.Port, "Port to listen to")
}

func toProfile(p string, s string) string {
	return fmt.Sprintf("%s.%s", p, s)
}
