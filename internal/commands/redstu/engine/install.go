package engine

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/redstu-project/redstu-client/internal/config"
	"gitlab.com/redstu-project/redstu-client/pkg/redstu"
	"gitlab.com/redstu-project/redstu-client/pkg/redstu/docker"
)

var (
	InstallCommand = &cobra.Command{
		Use:   "install [FLAGS]",
		Short: "Pull the engine's docker image",
		Long:  "Pull the engine's docker image",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
			repository := viper.GetString(toProfile(redstu.ConfigurationDefaultProfile, redstu.ConfigurationRepository))
			tag := viper.GetString(toProfile(redstu.ConfigurationDefaultProfile, redstu.ConfigurationImageTag))
			if err := docker.GetImage(repository, tag); err != nil {
				panic(err)
			}
		},
	}
)

func init() {
	InstallCommand.Flags().StringVar(&config.Params.EngineInstall.Image, "image", config.Params.EngineInstall.Image, "Define image repository")
	InstallCommand.Flags().StringVar(&config.Params.EngineInstall.Tag, "tag", config.Params.EngineInstall.Tag, "Define image tag")
	InstallCommand.Flags().StringVar(&config.Params.EngineInstall.Username, "user", config.Params.EngineInstall.Username, "Define username")
	InstallCommand.Flags().StringVar(&config.Params.EngineInstall.Password, "password", config.Params.EngineInstall.Password, "define password")
}

func newKey(profile string, key string) string {
	return fmt.Sprintf("%s.%s", profile, key)
}

func toProfile(p string, s string) string {
	return fmt.Sprintf("%s.%s", p, s)
}
