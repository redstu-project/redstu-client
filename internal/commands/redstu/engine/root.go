package engine

import (
	"github.com/spf13/cobra"
)

var (
	name    string
	tag     string
	Command = &cobra.Command{
		Use:   "engine",
		Short: "Manage Redstu Engine",
		Long:  "Manage Redstu Engine",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {

		},
	}
)

func init() {
	Command.AddCommand(InstallCommand)
}
