package redstu

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	projectCmd "gitlab.com/redstu-project/redstu-client/internal/commands/redstu/project"
	"gitlab.com/redstu-project/redstu-client/internal/config"
	"gitlab.com/redstu-project/redstu-client/pkg/redstu"

	"gitlab.com/redstu-project/redstu-client/internal/commands/redstu/completion"
	"gitlab.com/redstu-project/redstu-client/internal/commands/redstu/console"
	"gitlab.com/redstu-project/redstu-client/internal/commands/redstu/engine"
	"gitlab.com/redstu-project/redstu-client/internal/commands/redstu/generate"
	"gitlab.com/redstu-project/redstu-client/internal/commands/redstu/setup"
	"gitlab.com/redstu-project/redstu-client/internal/commands/redstu/state"
	"gitlab.com/redstu-project/redstu-client/internal/commands/redstu/up"
	"gitlab.com/redstu-project/redstu-client/internal/commands/redstu/version"

	configCmd "gitlab.com/redstu-project/redstu-client/internal/commands/redstu/config"

	"fmt"
	"log"
	"runtime"
)

var (
	newSetup = redstu.NewSetup()
)

var (
	RootCmd = &cobra.Command{
		Use:   "redstu",
		Short: "An utility for managing API mock project",
		Long:  "An utility for managing API mock project",
	}
)

func init() {
	RootCmd.PersistentFlags().StringVarP(&config.Params.Global.Profile, "profile", "p", config.Params.Global.Profile, "Profile")
	RootCmd.PersistentFlags().BoolVarP(&config.Params.Global.Verbose, "verbose", "v", false, "Verbose")
	RootCmd.PersistentFlags().StringVarP(&config.Params.Global.ConfigurationFile, "config", "c", "", "Config file")

	RootCmd.AddCommand(projectCmd.InitCommand)
	RootCmd.AddCommand(setup.Command)
	RootCmd.AddCommand(version.Command)
	RootCmd.AddCommand(completion.Command)

	enable(false, func() {
		RootCmd.AddCommand(configCmd.Command)
		RootCmd.AddCommand(console.Command)
		RootCmd.AddCommand(state.Command)
		RootCmd.AddCommand(generate.Command)
	})

	RootCmd.AddCommand(engine.Command)

	if runtime.GOOS != "windows" {
		RootCmd.AddCommand(up.Command)
	}

	cobra.OnInitialize(initConfig)
}

func enable(toggle bool, feature func()) {
	if toggle {
		feature()
	}
}

func initConfig() {
	if config.Params.Global.ConfigurationFile != "" {
		viper.SetConfigFile(config.Params.Global.ConfigurationFile)
	} else {
		viper.SetConfigName(redstu.ConfigurationFileName)
		viper.SetConfigType(redstu.ConfigurationFileFormat)

		for _, path := range redstu.ConfigurationFilePaths {
			viper.AddConfigPath(path)
		}
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			initDefaultProfile(config.Params.Global.Profile)
		} else {
			log.Println("Unknown error")
		}
	} else {
		profile := config.Params.Global.Profile
		if _, ok := viper.AllSettings()[profile]; ok {
			viper.Set("profile", profile)
		} else {
			fmt.Println("Didn't find profile", profile)
		}
	}
}

func Execute() {
	RootCmd.Execute()
}

func toProfile(p string, s string) string {
	return fmt.Sprintf("%s.%s", p, s)
}

func initDefaultProfile(profile string) {
	viper.SetDefault(toProfile(profile, redstu.ConfigurationRepository), newSetup[redstu.ConfigurationDefaultProfile].Repository)
	viper.SetDefault(toProfile(profile, redstu.ConfigurationImageTag), newSetup[redstu.ConfigurationDefaultProfile].Tag)
	viper.SetDefault(toProfile(profile, redstu.ConfigurationAuth), newSetup[redstu.ConfigurationDefaultProfile].Auth)
}
