package config

import (
	"github.com/spf13/cobra"
)

var (
	Command = &cobra.Command{
		Use:   "config",
		Short: "Manage global configurations",
		Long:  "Manage global configurations",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {

		},
	}
)

func init() {
	Command.AddCommand(GetCommand)
	Command.AddCommand(SetCommand)
}
