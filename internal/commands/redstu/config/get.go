package config

import (
	"github.com/spf13/cobra"
	"gitlab.com/redstu-project/redstu-client/internal/config"
)

var (
	all        string
	GetCommand = &cobra.Command{
		Use:   "get",
		Short: "Get a global configuration",
		Long:  "Get a global configuration",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {

		},
	}
)

func init() {
	GetCommand.Flags().BoolVarP(&config.Params.Config.All, "all", "a", config.Params.Config.All, "Get all configurations ")
}
