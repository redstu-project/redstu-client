package config

import (
	"github.com/spf13/cobra"
)

var (
	SetCommand = &cobra.Command{
		Use:   "set",
		Short: "Set a global configuration",
		Long:  "Set a global configuration",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {

		},
	}
)
