package state

import (
	"github.com/spf13/cobra"
	"gitlab.com/redstu-project/redstu-client/internal/config"
)

var (
	DumpCommand = &cobra.Command{
		Use:   "dump",
		Short: "dump",
		Long:  "dump",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
		},
	}
)

func init() {
	DumpCommand.Flags().StringVar(&config.Params.State.Dump.Name, "name", config.Params.State.Dump.Name, "Dump name")
	DumpCommand.Flags().StringVar(&config.Params.State.Dump.Description, "description", config.Params.State.Dump.Name, "Dump description")
}
