package state

import (
	"github.com/spf13/cobra"
)

var (
	Command = &cobra.Command{
		Use:   "state",
		Short: "state",
		Long:  "state",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {

		},
	}
)

func init() {
	Command.AddCommand(DumpCommand)
	Command.AddCommand(ListCommand)
	Command.AddCommand(RestoreCommand)
	Command.AddCommand(CreateCommand)
}
