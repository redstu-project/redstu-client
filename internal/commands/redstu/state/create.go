package state

import (
	"github.com/spf13/cobra"
	"gitlab.com/redstu-project/redstu-client/internal/config"
)

var (
	CreateCommand = &cobra.Command{
		Use:   "create",
		Short: "create",
		Long:  "create",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
		},
	}
)

func init() {
	CreateCommand.Flags().StringVar(&config.Params.State.Dump.Name, "name", config.Params.State.Dump.Name, "Dump name")
	CreateCommand.Flags().StringVar(&config.Params.State.Dump.Description, "description", config.Params.State.Dump.Name, "Dump description")
}
