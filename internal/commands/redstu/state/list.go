package state

import (
	"github.com/spf13/cobra"
	"gitlab.com/redstu-project/redstu-client/internal/config"
)

var (
	ListCommand = &cobra.Command{
		Use:   "list",
		Short: "list",
		Long:  "list",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
		},
	}
)

func init() {
	ListCommand.Flags().StringVar(&config.Params.State.Dump.Name, "name", config.Params.State.Dump.Name, "Dump name")
	ListCommand.Flags().StringVar(&config.Params.State.Dump.Description, "description", config.Params.State.Dump.Name, "Dump description")
}
