package state

import (
	"github.com/spf13/cobra"
	"gitlab.com/redstu-project/redstu-client/internal/config"
)

var (
	RestoreCommand = &cobra.Command{
		Use:   "restore",
		Short: "restore",
		Long:  "restore",
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
		},
	}
)

func init() {
	RestoreCommand.Flags().StringVar(&config.Params.State.Dump.Name, "name", config.Params.State.Dump.Name, "Dump name")
	RestoreCommand.Flags().StringVar(&config.Params.State.Dump.Description, "description", config.Params.State.Dump.Name, "Dump description")
}
