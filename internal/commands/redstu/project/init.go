package project

import (
	"bytes"
	"errors"
	"fmt"
	gitconfig "github.com/go-git/go-git/v5/config"
	"github.com/markbates/pkger"
	"github.com/spf13/afero"
	"github.com/spf13/cobra"
	"gitlab.com/redstu-project/redstu-client/internal/config"
	"gitlab.com/redstu-project/redstu-client/internal/logger"
	"gitlab.com/redstu-project/redstu-client/pkg/redstu/openapi3"
	"gitlab.com/redstu-project/redstu-client/pkg/redstu/project"
	"io"
	"os"
	"path/filepath"
	"text/template"
)

var (
	fs          = afero.NewOsFs()
	InitCommand = &cobra.Command{
		Use:   "init",
		Short: "Create a mock project",
		Long:  "Create a mock project",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) > 1 {
				return errors.New("Should have at most 1 path")
			}

			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
			defer logger.DefaultLogger.Sync()

			var projectPath string

			if len(args) == 0 {
				path, err := os.Getwd()
				if err != nil {
					logger.DefaultLogger.Fatal("Cannot get CWD")
				}

				projectPath = path
			} else {
				absolutePath, err := filepath.Abs(args[0])
				if err != nil {
					logger.DefaultLogger.Fatal("Not a valid path")
				}

				projectPath = absolutePath
			}

			projectPathInfo, err := fs.Stat(projectPath)
			if os.IsExist(err) {
				if projectPathInfo.IsDir() {
					logger.DefaultLogger.Debug(fmt.Sprintf("%s is a dir", projectPath))
				} else {
					logger.DefaultLogger.Debug(fmt.Sprintf("%s already exists and is a file", projectPath))
				}
			} else {
				if fs.MkdirAll(projectPath, 0755) != nil {
					logger.DefaultLogger.Fatal(fmt.Sprintf("Unable to create %s", projectPath))
				}

				logger.DefaultLogger.Debug(fmt.Sprintf("Created %s\n", projectPath))
			}

			newProject := *project.DefaultProject

			newProject.Override(
				project.WithPath(projectPath),
			)

			if config.Params.Init.Name != "" {
				newProject.Override(
					project.WithName(config.Params.Init.Name),
				)
			}

			if config.Params.Init.Engine != "" {
				newProject.Override(
					project.WithEngine(config.Params.Init.Engine),
				)
			}

			if config.Params.Init.EntryFile != "" {
				newProject.Override(
					project.WithEntryFile(config.Params.Init.EntryFile),
				)
			}

			if config.Params.Init.Version != "" {
				newProject.Override(
					project.WithVersion(config.Params.Init.Version),
				)
			}

			if config.Params.Init.Author != "" {
				newProject.Override(
					project.WithAuthor(config.Params.Init.Author),
				)
			}

			if config.Params.Init.Email != "" {
				newProject.Override(
					project.WithEmail(config.Params.Init.Email),
				)
			}

			if config.Params.Init.JSONSchemasPath != "" {
				newProject.Override(
					project.WithJSONSchemasPath(config.Params.Init.JSONSchemasPath),
				)
			}

			if config.Params.Init.XMLSchemasPath != "" {
				newProject.Override(
					project.WithXMLSchemasPath(config.Params.Init.XMLSchemasPath),
				)
			}

			if config.Params.Init.From != "" {
				from := config.Params.Init.From

				requests, err := openapi3.Generate(from)

				f, err := pkger.Open("/tmpl/openapi3/request.js.tmpl")
				if err != nil {
					panic(err)
				}

				var buf bytes.Buffer

				if _, err := io.Copy(&buf, f); err != nil {
					panic(err)
				}

				t, err := template.New("request").Parse(buf.String())
				if err != nil {
					panic(err)
				}

				var outputBuffer bytes.Buffer
				err = t.Execute(&outputBuffer, requests)
				if err != nil {
					panic(err)
				}

				if err := newProject.InitializeWithContent(&outputBuffer); err != nil {
					fmt.Println(err)
				} else {
					fullPath, err := os.Getwd()
					if err != nil {
						panic(err)
					}

					relativeDir, err := filepath.Rel(fullPath, newProject.Path)
					if err != nil {
						panic(err)
					}

					fmt.Printf("\nSuccessully created a redstu mock project!")
					fmt.Printf("Execute the following commands in order to get started")
					if fullPath != newProject.Path {
						fmt.Println("cd", relativeDir)
					}

					fmt.Println("npm install")
					fmt.Println("redstu run")
				}
			} else {
				if err := newProject.Initialize(); err != nil {
					fmt.Println(err)
				} else {
					fullPath, err := os.Getwd()
					if err != nil {
						panic(err)
					}

					relativeDir, err := filepath.Rel(fullPath, newProject.Path)
					if err != nil {
						panic(err)
					}

					fmt.Printf("\nSuccessully created a redstu mock project!")
					fmt.Printf("Execute the following commands in order to get started")
					if fullPath != newProject.Path {
						fmt.Println("cd", relativeDir)
					}

					fmt.Println("npm install")
					fmt.Println("redstu run")
				}

			}

		},
	}
)

func init() {
	if gitGlobalCfg, err := gitconfig.LoadConfig(gitconfig.GlobalScope); err == nil {
		config.Params.Init.Author = gitGlobalCfg.User.Name
		config.Params.Init.Email = gitGlobalCfg.User.Email
	}

	InitCommand.Flags().StringVar(&config.Params.Init.Name, "name", config.Params.Init.Name, "Define project name")
	InitCommand.Flags().StringVar(&config.Params.Init.Author, "author", config.Params.Init.Author, "Define author name")
	InitCommand.Flags().StringVar(&config.Params.Init.Email, "email", config.Params.Init.Email, "Define author email")
	InitCommand.Flags().StringVar(&config.Params.Init.Engine, "engine", config.Params.Init.Engine, "Define engine version")
	InitCommand.Flags().StringVarP(&config.Params.Init.EntryFile, "entry-file", "e", config.Params.Init.EntryFile, "Define entry file")
	InitCommand.Flags().StringVarP(&config.Params.Init.JSONSchemasPath, "json-schemas", "j", config.Params.Init.JSONSchemasPath, "Define where to store JSON schema files")
	InitCommand.Flags().StringVarP(&config.Params.Init.XMLSchemasPath, "xml-schemas", "x", config.Params.Init.XMLSchemasPath, "Define where to store XML schema files")
	InitCommand.Flags().StringVar(&config.Params.Init.Version, "version", config.Params.Init.Version, "Define the project version")
	InitCommand.Flags().StringVarP(&config.Params.Init.From, "from", "f", config.Params.Init.From, "Define a OpenAPI Specification to create the mock project from")
}
