package generate

import (
	"errors"
	"github.com/spf13/cobra"
	_ "gitlab.com/redstu-project/redstu-client/internal/config"
)

var (
	Command = &cobra.Command{
		Use:   "generate",
		Short: "Code generation for redstu",
		Long:  "Code generation for redstu",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) > 1 {
				return errors.New("Should have at most 1 path")
			}

			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
		},
	}
)

func init() {
	Command.AddCommand(FilterCommand)
	Command.AddCommand(HandlerCommand)
}
