package generate

import (
	"errors"
	"github.com/spf13/cobra"
	"gitlab.com/redstu-project/redstu-client/internal/config"
)

var (
	HandlerCommand = &cobra.Command{
		Use:   "handler",
		Short: "Generate handler",
		Long:  "Generate handler",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) > 1 {
				return errors.New("Should have at most 1 path")
			}

			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
		},
	}
)

func init() {
	HandlerCommand.Flags().StringArrayVarP(&config.Params.Generate.Handler.QueryParams, "query-params", "q", config.Params.Generate.Handler.QueryParams, "Query parameters")
	HandlerCommand.Flags().StringArrayVarP(&config.Params.Generate.Handler.Cookies, "cookies", "k", config.Params.Generate.Handler.Cookies, "Cookies")
	HandlerCommand.Flags().StringArrayVarP(&config.Params.Generate.Handler.Headers, "headers", "H", config.Params.Generate.Handler.Headers, "Headers")
	HandlerCommand.Flags().StringArrayVarP(&config.Params.Generate.Handler.Form, "form", "F", config.Params.Generate.Handler.Form, "Form")
	HandlerCommand.Flags().StringVarP(&config.Params.Generate.Handler.Method, "method", "m", config.Params.Generate.Handler.Method, "HTTP verb")
	HandlerCommand.Flags().BoolVarP(&config.Params.Generate.Handler.DryRun, "dry-run", "r", config.Params.Generate.Handler.DryRun, "Dry run")
}
