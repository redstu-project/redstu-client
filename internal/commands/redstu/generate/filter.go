package generate

import (
	"errors"
	"github.com/spf13/cobra"
	"gitlab.com/redstu-project/redstu-client/internal/config"
)

var (
	FilterCommand = &cobra.Command{
		Use:   "filter",
		Short: "Generate a filter",
		Long:  "Generate a filter",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) > 1 {
				return errors.New("Should have at most 1 path")
			}

			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
		},
	}
)

func init() {
	FilterCommand.Flags().StringArrayVarP(&config.Params.Generate.Filter.Methods, "methods", "m", config.Params.Generate.Filter.Methods, "HTTP verbs")
	FilterCommand.Flags().BoolVar(&config.Params.Generate.Filter.Any, "any", config.Params.Generate.Filter.Any, "All HTTP verbs")
	FilterCommand.Flags().BoolVarP(&config.Params.Generate.Handler.DryRun, "dry-run", "r", config.Params.Generate.Handler.DryRun, "Dry run")
}
