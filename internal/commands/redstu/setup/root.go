package setup

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/redstu-project/redstu-client/internal/config"
	"gitlab.com/redstu-project/redstu-client/pkg/redstu"

	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/afero"
	"gopkg.in/yaml.v2"
	"os"
)

type setupParams struct {
}

var (
	fs      = afero.NewOsFs()
	Command = &cobra.Command{
		Use:   "setup [FLAGS]",
		Short: "Create the configuration file",
		Long:  "Create the configuration file",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) > 1 {
				return errors.New("Should have at most 1 path")
			}

			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
			newSetup := redstu.NewSetup()
			newSetupBytes, _ := yaml.Marshal(newSetup)
			fmt.Println(string(newSetupBytes))
			fmt.Println(viper.AllSettings())

			if data, err := prompt(); err != nil {
				fmt.Println(err)
			} else {
				settings := viper.AllSettings()
				delete(settings, "profile")
				delete(settings, config.Params.Global.Profile)
				settings[config.Params.Global.Profile] = data.toMap()
				viper.Reset()
				viper.MergeConfigMap(settings)

				x, _ := yaml.Marshal(viper.AllSettings())
				fmt.Println("\n", string(x))

				viper.WriteConfigAs(os.ExpandEnv(fmt.Sprintf("%s/config.yml", data.Path)))
			}
		},
	}
)

func init() {
	Command.Flags().BoolVarP(&config.Params.Setup.Interactive, "interactive", "i", config.Params.Setup.Interactive, "Run setup interactively")
	Command.Flags().BoolVarP(&config.Params.Setup.Silent, "silent", "s", config.Params.Setup.Silent, "Run non-interactively")
	Command.Flags().StringVarP(&config.Params.Setup.Output, "output", "o", config.Params.Setup.Output, "File where the configuration will be written")
}

type promptData struct {
	Path       string
	Repository string
	Tag        string
	Auth       bool
	Username   string
	Password   string
}

func (p promptData) toMap() map[string]interface{} {
	newMap := map[string]interface{}{
		"image": p.Repository,
		"tag":   p.Tag,
		"auth":  p.Auth,
	}

	if p.Auth {
		newMap["credentials"] = map[string]interface{}{
			"user":     p.Username,
			"password": p.Password,
		}
	}

	return newMap
}

func prompt() (*promptData, error) {
	var repositoryInput *survey.Input
	if viper.IsSet(newKey(config.Params.Global.Profile, redstu.ConfigurationRepository)) {
		repositoryInput = &survey.Input{
			Message: "What's the Docker image?",
			Default: viper.GetString(newKey(config.Params.Global.Profile, redstu.ConfigurationRepository)),
		}
	} else {
		repositoryInput = &survey.Input{
			Message: "What's the Docker image?",
		}
	}
	repositoryQuestion := &survey.Question{
		Name:     "repository",
		Prompt:   repositoryInput,
		Validate: survey.Required,
	}

	var tagInput *survey.Input
	if viper.IsSet(newKey(config.Params.Global.Profile, redstu.ConfigurationImageTag)) {
		tagInput = &survey.Input{
			Message: "What's the Docker image tag?",
			Default: viper.GetString(newKey(config.Params.Global.Profile, redstu.ConfigurationImageTag)),
		}
	} else {
		tagInput = &survey.Input{
			Message: "What's the Docker image tag?",
		}
	}

	tagQuestion := &survey.Question{
		Name:     "tag",
		Prompt:   tagInput,
		Validate: survey.Required,
	}

	var imagePrompt = []*survey.Question{
		{
			Name: "Path",
			Prompt: &survey.Select{
				Message: "Where do you want to save the config file?",
				Options: []string{
					".",
					os.ExpandEnv("$HOME/.config/redstu/"),
					os.ExpandEnv("$HOME/.redstu/"),
					"/usr/local/etc/redstu",
					"/etc/redstu",
				},
				Default: os.ExpandEnv("$HOME/.config/redstu/"),
			},
		},
		repositoryQuestion,
		tagQuestion,
		{
			Name: "auth",
			Prompt: &survey.Confirm{
				Message: "Do this Docker image need authentication?",
			},
			Validate: survey.Required,
		},
	}

	var usernameInput *survey.Input
	if viper.IsSet(newKey(config.Params.Global.Profile, redstu.ConfigurationUser)) {
		usernameInput = &survey.Input{
			Message: "What's your username:",
			Default: viper.GetString(newKey(config.Params.Global.Profile, redstu.ConfigurationUser)),
		}
	} else {
		usernameInput = &survey.Input{
			Message: "What's your username:",
		}
	}

	var passwordInput *survey.Password
	if viper.IsSet(newKey(config.Params.Global.Profile, redstu.ConfigurationPassword)) {
		passwordInput = &survey.Password{
			Message: "What's your password?",
		}
	} else {
		passwordInput = &survey.Password{
			Message: "What's your password?:",
		}
	}

	var credentialsPrompt = []*survey.Question{
		{
			Name:     "username",
			Prompt:   usernameInput,
			Validate: survey.Required,
		},
		{
			Name:     "password",
			Prompt:   passwordInput,
			Validate: survey.Required,
		},
	}

	data := &promptData{}

	err := survey.Ask(imagePrompt, data, survey.WithHelpInput('^'))
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	if data.Auth {
		err := survey.Ask(credentialsPrompt, data, survey.WithHelpInput('^'))
		if err != nil {
			fmt.Println(err.Error())
			return nil, err
		}
	}

	return data, nil
}

func newKey(profile string, key string) string {
	return fmt.Sprintf("%s.%s", profile, key)
}
