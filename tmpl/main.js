import { state, Expect, MediaTypes } from '@redstu/redstu-js';

state.put("website","https://redstu.org");

console.log("Hello world");

console.log(state);

Expect.getRequest("/website",(req, resp) => {
    console.log("Logging website request");
    resp.setStatus(200);
    resp.setContentType(MediaTypes.TEXT_PLAIN);
    resp.sendText(state.get("website").toString());
});

Expect.getRequest("/get/{name}",(req, resp) => {
    console.log(req.pathParams().getString("name"));
    resp.setContentType(MediaTypes.APPLICATION_JSON);
    resp.sendObject(req.pathParams());
});

Expect.getRequest("/empty",(req, resp) => {
    resp.setStatus(422);
    resp.send();
});

