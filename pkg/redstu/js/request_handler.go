package js

type RequestHandler struct {
	Description string
	Summary     string
	Method      string
	Path        string
	PathParams  []string
	QueryParams []string
	Cookies     []string
	Headers     []string
}

func NewRequestHandler() *RequestHandler {
	return &RequestHandler{
		PathParams:  []string{},
		QueryParams: []string{},
		Cookies:     []string{},
		Headers:     []string{},
	}
}

func newRequestHandler(method string) *RequestHandler {
	return &RequestHandler{
		Method:      method,
		PathParams:  []string{},
		QueryParams: []string{},
		Cookies:     []string{},
		Headers:     []string{},
	}
}

func GetRequestHandler() *RequestHandler {
	return newRequestHandler("GET")
}

func PostRequestHandler() *RequestHandler {
	return newRequestHandler("POST")
}

func PutRequestHandler() *RequestHandler {
	return newRequestHandler("PUT")
}

func DeleteRequestHandler() *RequestHandler {
	return newRequestHandler("DELETE")
}

func PatchRequestHandler() *RequestHandler {
	return newRequestHandler("PATCH")
}

func TraceRequestHandler() *RequestHandler {
	return newRequestHandler("TRACE")
}

func HeadRequestHandler() *RequestHandler {
	return newRequestHandler("HEAD")
}

func ConnectRequestHandler() *RequestHandler {
	return newRequestHandler("CONNECT")
}

func OptionsRequestHandler() *RequestHandler {
	return newRequestHandler("OPTIONS")
}
