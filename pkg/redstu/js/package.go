package js

type packageJSON struct {
	Name            string           `json:"name,omitempty"`
	Version         string           `json:"version,omitempty"`
	Main            string           `json:"main,omitempty"`
	Author          string           `json:"author,omitempty"`
	DevDependencies *devDependencies `json:"devDependencies,omitempty"`
}

type devDependencies struct {
	Redstu string `json:"@redstu/redstu-js,omitempty"`
}

func NewPackage() *packageJSON {
	return &packageJSON{
		DevDependencies: &devDependencies{},
	}
}
