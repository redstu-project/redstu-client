package js

type RequestFilter struct {
	Methods     string
	PathPattern string
}
