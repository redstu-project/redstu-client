package js

var EntryFile string = `
import { state, Respond, MediaTypes, log } from '@redstu/redstu-js';

state["website"] = "https://redstu.org";
state["person"] = { name: "Mario", surname : "Junior", phones: ["842538083", "849902228"], address: { province: "Maputo", district: "Boane"}};

Respond.getRequest("/website", (req, resp) => {
    log.info("Logging website request");
    resp.setStatus(200);
    resp.setContentType(MediaTypes.TEXT_PLAIN);
    resp.sendText(state.website);
});

Respond.postRequest("/name",  async (req, resp) => {
    state.person.name = await req.body.text();
    state.person.dummy = req.body.object();
    resp.send();
});

Respond.getRequest("/empty", (req, resp) => {
    resp.setStatus(422);
    resp.send();
});

let loremSignal = {name: "Lorem", description: "This is just a dummy signal", parameterized: true};
Respond.signal(loremSignal,(object) => {
   console.log("Lorem signal was fired: "+JSON.stringify(object));
});
`
