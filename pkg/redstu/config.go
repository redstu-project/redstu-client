package redstu

var (
	ConfigurationRepository = "image"
	ConfigurationImageTag   = "tag"
	ConfigurationUser       = "credentials.user"
	ConfigurationPassword   = "credentials.password"
	ConfigurationAuth       = "auth"

	ConfigurationFileFormat     = "yml"
	ConfigurationFileName       = "config"
	ConfigurationFilePaths      = []string{"/etc/redstu", "/usr/local/etc/redstu", "$HOME/.config/redstu/", ".", "$HOME/.redstu/"}
	ConfigurationDefaultProfile = "default"
)

type Setup map[string]*configuration

type configuration struct {
	Repository  string       `json:"image,omitempty" yaml:"image,omitempty"`
	Tag         string       `json:"tag,omitempty" yaml:"tag,omitempty"`
	Auth        bool         `json:"auth" yaml:"auth"`
	Credentials *credentials `json:"credentials,omitempty" yaml:"credentials,omitempty"`
}

type credentials struct {
	Username string `json:"username,omitempty" yaml:"username,omitempty"`
	Password string `json:"password,omitempty" yaml:"password,omitempty"`
}

func NewSetup() Setup {
	return map[string]*configuration{
		ConfigurationDefaultProfile: NewConfiguration(),
	}
}

func NewConfiguration() *configuration {
	return &configuration{
		Repository:  "registry.gitlab.com/redstu-project/redstu-engine",
		Tag:         "develop",
		Auth:        false,
		Credentials: nil,
	}
}
