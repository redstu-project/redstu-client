package project

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/spf13/afero"
	"gitlab.com/redstu-project/redstu-client/pkg/redstu/js"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"text/template"
)

const internalDir string = ".redstu"
const defaultStatePath string = ".redstu/states/default.json"
const alternativeStatePath string = "src/state.json"
const defaultSignalsPath string = ".redstu/signals/default.json"
const gitIgnorePath string = ".gitignore"
const packageJson string = "package.json"
const manifestFilePath string = "redstu.yml"

var (
	DefaultProject = &Project{
		Name:      "sample-project",
		EntryFile: "src/main.js",
	}
	fs = afero.NewOsFs()
)

func New(options ...ProjectOption) *Project {
	defaultProject := *DefaultProject

	defaultProject.EntryFile = entryFile

	for _, option := range options {
		option(&defaultProject)
	}

	return &defaultProject
}

func (p *Project) Override(options ...ProjectOption) {
	for _, option := range options {
		option(p)
	}
}

func WithPath(path string) ProjectOption {
	return func(p *Project) {
		p.Path = path
	}
}

func WithEntryFile(entry string) ProjectOption {
	return func(p *Project) {
		p.EntryFile = entry
	}
}

func WithName(name string) ProjectOption {
	return func(p *Project) {
		p.Name = name
	}
}

func WithVersion(version string) ProjectOption {
	return func(p *Project) {
		p.Version = version
	}
}

func WithEngine(engine string) ProjectOption {
	return func(p *Project) {
		p.Engine = engine
	}
}

func WithEmail(email string) ProjectOption {
	return func(p *Project) {
		p.Email = email
	}
}

func WithAuthor(author string) ProjectOption {
	return func(p *Project) {
		p.Author = author
	}
}

func WithJSONSchemasPath(path string) ProjectOption {
	return func(p *Project) {
		p.JSONSchemasPath = path
	}
}

func WithXMLSchemasPath(path string) ProjectOption {
	return func(p *Project) {
		p.XMLSchemasPath = path
	}
}

func (p *Project) createMainJsDir() error {
	mainJsPath := path.Join(p.Path, p.EntryFile)
	mainJsDir := path.Dir(mainJsPath)

	if info, err := fs.Stat(mainJsDir); err != nil {
		if os.IsNotExist(err) {
			if err := fs.MkdirAll(mainJsDir, 0755); err != nil {
				return err
			}
		}
	} else if info.IsDir() {
		return err
	} else {
		return err
	}

	return nil
}

func (p *Project) createInternalDir() error {
	internalDirAbs := path.Join(p.Path, internalDir)
	if info, err := fs.Stat(internalDirAbs); err != nil {
		if os.IsNotExist(err) {
			if err := fs.MkdirAll(internalDirAbs, 0755); err != nil {
				return err
			}
		}
	} else if info.IsDir() {
		return err
	} else {
		return err
	}

	return nil
}

func (p *Project) createProjectManifest() (string, error) {
	redstuYmlPath := path.Join(p.Path, "redstu.yml")
	if info, err := fs.Stat(redstuYmlPath); err != nil {
		if os.IsNotExist(err) {
			if _, err := fs.Create(redstuYmlPath); err != nil {
				return "", err
			}
		}
	} else if info.IsDir() {
		return "", err
	} else {
		return "", err
	}

	return redstuYmlPath, nil
}

func (p *Project) createProjectFile(file string) (string, error) {
	filePath := path.Join(p.Path, file)

	if info, err := fs.Stat(filePath); err != nil {
		if os.IsNotExist(err) {
			if err := fs.MkdirAll(filepath.Dir(filePath), 0770); err != nil {
				return "", err
			}

			if _, err := fs.Create(filePath); err != nil {
				return "", err
			}
		}

	} else if info.IsDir() {
		return "", err
	} else {
		return "", err
	}

	return filePath, nil
}

func (p *Project) writePackageJSON(file string) error {
	manifest := js.NewPackage()
	manifest.Name = p.Name
	manifest.Author = fmt.Sprintf("%s (%s)", p.Author, p.Email)
	manifest.Version = fmt.Sprintf("%s", p.Version)
	manifest.Main = fmt.Sprintf("%s", p.EntryFile)
	manifest.DevDependencies.Redstu = "^1.0.0-dev"

	d, err := json.MarshalIndent(manifest, "", "  ")
	if err != nil {
		return err
	}

	if err := afero.WriteFile(fs, file, d, 0644); err != nil {
		return err
	}

	return nil
}

func (p *Project) writeManifest(path string, config *Manifest) error {
	config.Override(
		WithProjectEngine(p.Engine),
		WithProjectVersion(p.Version),
		WithProjectAuthor(p.Author),
		WithProjectEmail(p.Email),
		WithProjectName(p.Name),
		WithProjectEntryFile(p.EntryFile),
		WithJSONSchemas(p.JSONSchemasPath),
		WithXMLSchemas(p.XMLSchemasPath),
	)

	d, err := yaml.Marshal(config)
	if err != nil {
		return err
	}

	if err := afero.WriteFile(fs, path, d, 0644); err != nil {
		return err
	}

	return nil
}

func (p *Project) createMainJsFile() (string, error) {
	mainJsPath := path.Join(p.Path, p.EntryFile)

	if _, err := fs.Create(mainJsPath); err != nil {
		return "", err
	}

	if err := afero.WriteFile(fs, mainJsPath, []byte(js.EntryFile), 0644); err != nil {
		return "", err
	}

	return p.EntryFile, nil
}

func (p *Project) createMainJsFileWithContent(buf *bytes.Buffer) (string, error) {
	mainJsPath := path.Join(p.Path, p.EntryFile)

	if _, err := fs.Create(mainJsPath); err != nil {
		return "", err
	}

	if err := afero.WriteFile(fs, mainJsPath, []byte(buf.String()), 0644); err != nil {
		return "", err
	}

	return p.EntryFile, nil
}

func (p *Project) Initialize() error {
	files := make([]string, 0)

	if empty, _ := p.isEmpty(); !empty {
		return errors.New("The directory shouldn't have any files")
	}

	fmt.Println("Generating", p.Name, "mock project")

	config := *DefaultManifest

	packageJSON, err := p.createProjectFile(packageJson)
	if err != nil {
		return err
	}

	if err := p.writePackageJSON(packageJSON); err != nil {
		return err
	}
	files = append(files, packageJson)

	if err := p.createMainJsDir(); err != nil {
		return err
	}

	mainJsPath, err := p.createMainJsFile()
	if err != nil {
		return err
	}
	files = append(files, mainJsPath)

	_, err = p.createProjectFile(alternativeStatePath)
	if err != nil {
		return err
	}
	files = append(files, alternativeStatePath)

	redstuYmlPath, err := p.createProjectManifest()
	if err != nil {
		return err
	}
	files = append(files, manifestFilePath)

	if err := p.writeManifest(redstuYmlPath, &config); err != nil {
		return err
	}

	if err = p.createInternalDir(); err != nil {
		return err
	}
	files = append(files, internalDir)

	_, err = p.createProjectFile(defaultStatePath)
	if err != nil {
		return err
	}
	files = append(files, defaultStatePath)

	_, err = p.createProjectFile(defaultSignalsPath)
	if err != nil {
		return err
	}
	files = append(files, defaultSignalsPath)

	_, err = p.createProjectFile(gitIgnorePath)
	if err != nil {
		return err
	}
	files = append(files, gitIgnorePath)

	createdFilesTemplate := `{{ range . }}Created: {{.}}{{"\n"}}{{ end }}`
	tmpl := template.Must(template.New("t1").Parse(createdFilesTemplate))
	if err := tmpl.Execute(os.Stdout, files); err != nil {
		return err
	}

	return nil
}

func (p *Project) InitializeWithContent(buf *bytes.Buffer) error {
	files := make([]string, 0)

	if empty, _ := p.isEmpty(); !empty {
		return errors.New("The directory shouldn't have any files")
	}

	fmt.Println("Generating", p.Name, "mock project")

	config := *DefaultManifest

	packageJSON, err := p.createProjectFile(packageJson)
	if err != nil {
		return err
	}

	if err := p.writePackageJSON(packageJSON); err != nil {
		return err
	}
	files = append(files, packageJson)

	if err := p.createMainJsDir(); err != nil {
		return err
	}

	mainJsPath, err := p.createMainJsFileWithContent(buf)
	if err != nil {
		return err
	}
	files = append(files, mainJsPath)

	_, err = p.createProjectFile(alternativeStatePath)
	if err != nil {
		return err
	}
	files = append(files, alternativeStatePath)

	redstuYmlPath, err := p.createProjectManifest()
	if err != nil {
		return err
	}
	files = append(files, manifestFilePath)

	if err := p.writeManifest(redstuYmlPath, &config); err != nil {
		return err
	}

	if err = p.createInternalDir(); err != nil {
		return err
	}
	files = append(files, internalDir)

	_, err = p.createProjectFile(defaultStatePath)
	if err != nil {
		return err
	}
	files = append(files, defaultStatePath)

	_, err = p.createProjectFile(defaultSignalsPath)
	if err != nil {
		return err
	}
	files = append(files, defaultSignalsPath)

	_, err = p.createProjectFile(gitIgnorePath)
	if err != nil {
		return err
	}
	files = append(files, gitIgnorePath)

	createdFilesTemplate := `{{ range . }}Created: {{.}}{{"\n"}}{{ end }}`
	tmpl := template.Must(template.New("t1").Parse(createdFilesTemplate))
	if err := tmpl.Execute(os.Stdout, files); err != nil {
		return err
	}

	return nil
}

func (p *Project) isEmpty() (bool, error) {
	entries, err := ioutil.ReadDir(p.Path)
	if err != nil {
		return false, err
	}

	return len(entries) == 0, nil
}
