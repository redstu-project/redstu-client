package project

import (
	"github.com/spf13/afero"
	"os"
	"path"
	"testing"
)

func Test_Project_Path(t *testing.T) {
	tests := []struct {
		name string
		path string
		want string
	}{
		{
			name: "Absolute Path",
			path: "/tmp",
			want: "/tmp",
		},
		{
			name: "Relative Path",
			path: "tmp",
			want: "tmp",
		},
		{
			name: "Empty Path",
			path: "",
			want: "",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			project := New(WithPath(test.path))
			if project.Path != test.want {
				t.Errorf("[%s] failed, wanted %s got %s", test.name, test.want, project.Path)
			}
		})
	}
}

func Test_Project_Initialization(t *testing.T) {
	fs = afero.NewMemMapFs()
	cwd, err := os.Getwd()
	if err != nil {
		t.Errorf("Can't get the current working directory")
	}

	projectPath := path.Join(cwd, "tmp")
	if err := fs.Mkdir(projectPath, 0755); err != nil {
		t.Errorf("can't create a new path %s", err)
	}

	project := New(WithPath(projectPath))

	if err := project.Initialize(); err != nil {
		// t.Errorf("Error %s while initializing the project", err)
	} else {
		t.Logf("Removing created files")

		if err := fs.RemoveAll(projectPath); err != nil {
			t.Errorf("Error while removing %s", projectPath)
		} else {
			t.Logf("%s removed successfully", projectPath)
		}
	}
}
