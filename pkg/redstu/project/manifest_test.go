package project

import (
	"testing"
)

type manifestFile struct {
	version     string
	entryFile   string
	jsonSchemas string
	xmlSchemas  string
	apiEnabled  bool
	maxAge      int32
}

func Test_Configuration_Initialization(t *testing.T) {
	tt := []struct {
		name   string
		config manifestFile
		want   manifestFile
	}{
		{
			name: "Version 2.0.0",
			config: manifestFile{
				version:     "2.0.0",
				entryFile:   "index.ts",
				jsonSchemas: "json",
				xmlSchemas:  "xml",
				apiEnabled:  true,
				maxAge:      0,
			},
			want: manifestFile{
				version:     "2.0.0",
				entryFile:   "index.ts",
				jsonSchemas: "json",
				xmlSchemas:  "xml",
				apiEnabled:  true,
				maxAge:      0,
			},
		},
		{
			name: "Version 1.0.0",
			config: manifestFile{
				version:     "1.0.0",
				entryFile:   "src/index.js",
				jsonSchemas: "json",
				xmlSchemas:  "xml",
				apiEnabled:  false,
				maxAge:      0,
			},
			want: manifestFile{
				version:     "1.0.0",
				entryFile:   "src/index.js",
				jsonSchemas: "json",
				xmlSchemas:  "xml",
				apiEnabled:  false,
				maxAge:      0,
			},
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			config := NewManifest(
				WithProjectVersion(test.config.version),
				WithProjectEntryFile(test.config.entryFile),
				WithJSONSchemas(test.config.jsonSchemas),
				WithXMLSchemas(test.config.xmlSchemas),
				WithAPIEnabled(test.config.apiEnabled),
				WithAPIMaxAge(test.config.maxAge),
			)

			if config.Version != test.want.version {
				t.Errorf("[%s] Failed wanted version %s got %s", test.name, test.want.version, config.Version)
			}

			if config.Paths.EntryFile != test.want.entryFile {
				t.Errorf("[%s] Failed wanted version %s got %s", test.name, test.want.entryFile, config.Paths.EntryFile)
			}

			if config.Paths.Schemas.JSON != test.want.jsonSchemas {
				t.Errorf("[%s] Failed wanted jsonSchemas %s got %s", test.name, test.want.jsonSchemas, config.Paths.Schemas.JSON)
			}

			if config.Paths.Schemas.XML != test.want.xmlSchemas {
				t.Errorf("[%s] Failed wanted xmlSchemas %s got %s", test.name, test.want.xmlSchemas, config.Paths.Schemas.XML)
			}

			if config.Servers.API.Enabled != test.want.apiEnabled {
				t.Errorf("[%s] Failed wanted enabled %t got %t", test.name, test.want.apiEnabled, config.Servers.API.Enabled)
			}

			if config.Servers.API.CORS.MaxAge != test.want.maxAge {
				t.Errorf("[%s] Failed wanted maxAge %d got %d", test.name, test.want.maxAge, config.Servers.API.CORS.MaxAge)
			}
		})
	}
}
