package project

type commandParams struct {
	Global        globalParams
	Init          initParams
	Setup         setupParams
	Up            upParams
	EngineInstall engineInstall
	Config        configParams
	Version       versionParams
	State         stateParams
	Generate      generateParams
}

type initParams struct {
	Engine          string
	Name            string
	EntryFile       string
	JSONSchemasPath string
	XMLSchemasPath  string
	Version         string
	From            string
	Author          string
	Email           string
}

type setupParams struct {
	Profile     string
	Silent      bool
	Interactive bool
	Output      string
}

type globalParams struct {
	Debug             bool
	Verbose           bool
	ConfigurationFile string
	Profile           string
}

type upParams struct {
	Address string
	Port    uint16
}

type engineInstall struct {
	Tag      string
	Image    string
	Username string
	Password string
}

type configParams struct {
	All bool
}

type versionParams struct {
	Format string
	JSON   bool
	YAML   bool
	XML    bool
	Text   bool
}

type stateCreateParams struct {
	From        string
	Description string
	Name        string
}

type stateUpdateParams struct {
	UUID        string
	Description string
	Name        string
}

type stateDeleteParams struct {
	UUID string
}

type stateParams struct {
	Create stateCreateParams
	Update stateUpdateParams
	Delete stateDeleteParams
	Dump   stateDumpParameters
}

type stateDumpParameters struct {
	Description string
	Name        string
}

type generateParams struct {
	Filter  generateFilterParams
	Handler generateHandlerParams
	Output  string
}

type generateFilterParams struct {
	Path    string
	Methods []string
	Any     bool
	Get     bool
	Post    bool
	Put     bool
	Delete  bool
	Patch   bool
	Connect bool
	Trace   bool
	Options bool
	DryRun  bool
}

type generateHandlerParams struct {
	Path        string
	Method      string
	Any         bool
	Get         bool
	Post        bool
	Put         bool
	Delete      bool
	Patch       bool
	Connect     bool
	Trace       bool
	Options     bool
	Headers     []string
	QueryParams []string
	Cookies     []string
	Form        []string
	DryRun      bool
}

func NewCommandParams() *commandParams {
	return &commandParams{
		Global: globalParams{
			Debug:             false,
			Verbose:           false,
			Profile:           "default",
			ConfigurationFile: "",
		},
		Setup: setupParams{
			Profile:     "default",
			Silent:      true,
			Interactive: false,
			Output:      "",
		},
		Init: initParams{
			Engine:          "v1",
			Name:            "hello-world",
			EntryFile:       "src/main.js",
			JSONSchemasPath: "schemas/json",
			XMLSchemasPath:  "schemas/xml",
			Version:         "1.0.0",
		},
		Up: upParams{
			Address: "localhost",
			Port:    8910,
		},
		EngineInstall: engineInstall{},
		Config:        configParams{},
		Version: versionParams{
			Format: "text",
			JSON:   false,
			YAML:   false,
			Text:   false,
			XML:    false,
		},
		State: stateParams{
			Dump: stateDumpParameters{
				Description: "",
				Name:        "",
			},
		},
		Generate: generateParams{
			Filter: generateFilterParams{
				Methods: []string{},
			},
			Handler: generateHandlerParams{
				Headers:     []string{},
				QueryParams: []string{},
				Cookies:     []string{},
				Form:        []string{},
			},
		},
	}
}

type flag struct {
	Long         string
	Short        string
	Description  string
	DefaultValue interface{}
}
