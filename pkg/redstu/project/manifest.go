package project

import (
	_ "encoding/json"
	"gopkg.in/yaml.v2"
)

const entryFile string = "main.js"

type author struct {
	Name  string `json:"name,omitempty" yaml:"name,omitempty"`
	Email string `json:"email,omitempty" yaml:"email,omitempty"`
}

type Project struct {
	Path            string
	Name            string
	Engine          string
	Version         string
	Author          string
	Email           string
	EntryFile       string
	JSONSchemasPath string
	XMLSchemasPath  string
}

type ProjectOption func(*Project)

type Manifest struct {
	Engine  string  `json:"engine,omitempty" yaml:"engine,omitempty"`
	Version string  `json:"-" yaml:"-"`
	Author  author  `json:"author,omitempty" yaml:"author,omitempty"`
	Project string  `json:"name,omitempty" yaml:"name,omitempty"`
	Paths   Paths   `json:"paths,omitempty" yaml:"paths,omitempty"`
	Servers Servers `json:"servers,omitempty" yaml:"servers,omitempty"`
}

type Paths struct {
	InitialState string  `json:"-" yaml:"-"`
	EntryFile    string  `json:"entryFile,omitempty" yaml:"entryFile,omitempty"`
	Schemas      Schemas `json:"-" yaml:"-"`
}

type Schemas struct {
	JSON string `json:"json,omitempty" yaml:"json,omitempty"`
	XML  string `json:"xml,omitempty" yaml:"xml,omitempty"`
}

type Servers struct {
	API  API  `json:"cockpit,omitempty" yaml:"cockpit,omitempty"`
	Mock Mock `json:"mock,omitempty" yaml:"mock,omitempty"`
}

type API struct {
	Enabled bool `json:"enabled,omitempty" yaml:"enabled,omitempty"`
	CORS    CORS `json:"cors,omitempty" yaml:"cors,omitempty"`
}

type Mock struct {
	CORS CORS `json:"cors,omitempty" yaml:"cors,omitempty"`
}

type CORS struct {
	AllowOrigin   string `json:"allowOrigin,omitempty" yaml:"allowOrigin,omitempty"`
	AllowMethods  string `json:"allowMethods,omitempty" yaml:"allowMethods,omitempty"`
	AllowHeaders  string `json:"allowHeaders,omitempty" yaml:"allowHeaders"`
	ExposeHeaders string `json:"exposeHeaders,omitempty" yaml:"exposeHeaders,omitempty"`
	MaxAge        int32  `json:"maxAge,omitempty" yaml:"maxAge,omitempty"`
}

type GlobalConfig struct {
	Registry    string      `json:"registry,omitempty" yaml:"registry,omitempty"`
	Image       Image       `json:"image,omitempty" yaml:"image,omitempty"`
	Credentials Credentials `json:"credentials,omitempty" yam:"credentials,omitempty"`
}

type Image struct {
	Name string `json:"name,omitempty" yaml:"name,omitempty"`
	Tag  string `json:"tag,omitempty" yaml:"tag,omitempty"`
}

type Credentials struct {
	User     string `json:"user,omitempty" yaml:"user,omitempty"`
	Password string `json:"password,omitempty" yaml:"password,omitempty"`
}

type GlobalConfigOption func(*GlobalConfig)

type ManifestOption func(*Manifest)

var (
	DefaultManifest = &Manifest{
		Engine:  "1.0.0-dev",
		Version: "1.0.0-dev",
		Project: "sample-project",
		Author:  author{},
		Paths: Paths{
			InitialState: ".redstu/states/default.json",
			EntryFile:    "src/main.js",
			Schemas: Schemas{
				JSON: "schemas/json",
				XML:  "schemas/xml",
			},
		},
		Servers: Servers{
			API: API{
				Enabled: true,
				CORS: CORS{
					AllowOrigin:   "*",
					AllowHeaders:  "*",
					AllowMethods:  "*",
					ExposeHeaders: "*",
					MaxAge:        86400,
				},
			},
			Mock: Mock{
				CORS: CORS{
					AllowOrigin:   "*",
					AllowHeaders:  "*",
					AllowMethods:  "*",
					ExposeHeaders: "*",
					MaxAge:        86400,
				},
			},
		},
	}
)

func LoadManifest(blob []byte) (*Manifest, error) {
	var manifest Manifest
	err := yaml.Unmarshal(blob, &manifest)
	return &manifest, err
}

func NewManifest(options ...ManifestOption) *Manifest {
	DefaultManifest := *DefaultManifest

	for _, option := range options {
		option(&DefaultManifest)
	}

	return &DefaultManifest
}

func (c *Manifest) Override(options ...ManifestOption) {
	for _, option := range options {
		option(c)
	}
}

func WithProjectEngine(engine string) ManifestOption {
	return func(config *Manifest) {
		config.Engine = engine
	}
}

func WithProjectAuthor(author string) ManifestOption {
	return func(config *Manifest) {
		config.Author.Name = author
	}
}

func WithProjectEmail(email string) ManifestOption {
	return func(config *Manifest) {
		config.Author.Email = email
	}
}

func WithProjectVersion(version string) ManifestOption {
	return func(config *Manifest) {
		config.Version = version
	}
}

func WithProjectName(name string) ManifestOption {
	return func(config *Manifest) {
		config.Project = name
	}
}

func WithProjectEntryFile(entry string) ManifestOption {
	return func(config *Manifest) {
		config.Paths.EntryFile = entry
	}
}

func WithJSONSchemas(path string) ManifestOption {
	return func(config *Manifest) {
		config.Paths.Schemas.JSON = path
	}
}

func WithXMLSchemas(path string) ManifestOption {
	return func(config *Manifest) {
		config.Paths.Schemas.XML = path
	}
}

func WithAPIEnabled(b bool) ManifestOption {
	return func(config *Manifest) {
		config.Servers.API.Enabled = b
	}
}

func WithAPIDisabled() ManifestOption {
	return func(config *Manifest) {
		config.Servers.API.Enabled = false
	}
}

func WithAPIAllowOrigin(origins string) ManifestOption {
	return func(config *Manifest) {
		config.Servers.API.CORS.AllowOrigin = origins
	}
}

func WithAPIAllowMethods(methods string) ManifestOption {
	return func(config *Manifest) {
		config.Servers.API.CORS.AllowMethods = methods
	}
}

func WithAPIAllowHeaders(headers string) ManifestOption {
	return func(config *Manifest) {
		config.Servers.API.CORS.AllowHeaders = headers
	}
}

func WithAPIExposeHeaders(headers string) ManifestOption {
	return func(config *Manifest) {
		config.Servers.API.CORS.ExposeHeaders = headers
	}
}

func WithAPIMaxAge(maxAge int32) ManifestOption {
	return func(config *Manifest) {
		config.Servers.API.CORS.MaxAge = maxAge
	}
}

func WithMockAllowOrigin(origins string) ManifestOption {
	return func(config *Manifest) {
		config.Servers.Mock.CORS.AllowOrigin = origins
	}
}

func WithMockAllowMethods(methods string) ManifestOption {
	return func(config *Manifest) {
		config.Servers.Mock.CORS.AllowMethods = methods
	}
}

func WithMockAllowHeaders(headers string) ManifestOption {
	return func(config *Manifest) {
		config.Servers.Mock.CORS.AllowHeaders = headers
	}
}

func WithMockExposeHeaders(headers string) ManifestOption {
	return func(config *Manifest) {
		config.Servers.Mock.CORS.ExposeHeaders = headers
	}
}

func WithMockMaxAge(maxAge int32) ManifestOption {
	return func(config *Manifest) {
		config.Servers.Mock.CORS.MaxAge = maxAge
	}
}
