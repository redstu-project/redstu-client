package openapi3

import (
	"github.com/getkin/kin-openapi/openapi3"
	"gitlab.com/redstu-project/redstu-client/pkg/redstu/js"
	"net/url"
)

func Generate(from string) ([]*js.RequestHandler, error) {
	newUrl, err := url.Parse(from)
	if err != nil {
		return []*js.RequestHandler{}, err
	}

	spec, err := openapi3.NewSwaggerLoader().LoadSwaggerFromURI(newUrl)
	if err != nil {
		return []*js.RequestHandler{}, err
	}

	requests := []*js.RequestHandler{}

	for name, path := range spec.Paths {
		var thisRequest *js.RequestHandler

		if operation := path.Get; operation != nil {
			thisRequest = createHandler(name, operation)
			thisRequest.Method = "GET"
		} else if operation := path.Post; operation != nil {
			thisRequest = createHandler(name, operation)
			thisRequest.Method = "POST"
		} else if operation := path.Put; operation != nil {
			thisRequest = createHandler(name, operation)
			thisRequest.Method = "PUT"
		} else if operation := path.Patch; operation != nil {
			thisRequest = createHandler(name, operation)
			thisRequest.Method = "PATCH"
		} else if operation := path.Delete; operation != nil {
			thisRequest = createHandler(name, operation)
			thisRequest.Method = "DELETE"
		} else if operation := path.Trace; operation != nil {
			thisRequest = createHandler(name, operation)
			thisRequest.Method = "TRACE"
		} else if operation := path.Connect; operation != nil {
			thisRequest.Method = "CONNECT"
			thisRequest = createHandler(name, operation)
		} else if operation := path.Head; operation != nil {
			thisRequest = createHandler(name, operation)
			thisRequest.Method = "HEAD"
		}
		requests = append(requests, thisRequest)
	}

	return requests, nil
}

func createHandler(path string, operation *openapi3.Operation) *js.RequestHandler {
	req := js.NewRequestHandler()

	req.Description = operation.Description
	req.Summary = operation.Summary
	req.Path = path

	for _, param := range operation.Parameters {
		if param.Value.In == "path" {
			req.PathParams = append(req.PathParams, param.Value.Name)
		} else if param.Value.In == "query" {
			req.QueryParams = append(req.QueryParams, param.Value.Name)
		} else if param.Value.In == "cookies" {
			req.Cookies = append(req.Cookies, param.Value.Name)
		}
	}

	return req
}
