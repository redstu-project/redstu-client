package docker

import (
	"context"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/jsonmessage"
	"github.com/moby/term"
	"os"
)

func GetImage(repo, tag string) error {
	ctx := context.Background()

	newClient, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return err
	}

	var image string

	if tag != "" {
		image = fmt.Sprintf("%s:%s", repo, tag)
	} else {
		image = repo
	}

	reader, err := newClient.ImagePull(ctx, image, types.ImagePullOptions{})
	if err != nil {
		return err
	}

	termFd, isTerm := term.GetFdInfo(os.Stderr)
	jsonmessage.DisplayJSONMessagesStream(reader, os.Stderr, termFd, isTerm, nil)

	return nil
}
