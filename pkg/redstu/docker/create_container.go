package docker

import (
	"context"
	"fmt"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"os"
)

type state struct {
	Container containerX `json:"container"`
}

type containerX struct {
	ID string `json:"id"`
}

func newState(name string) *state {
	return &state{
		Container: containerX{
			ID: name,
		},
	}
}

func CreateContainer(name, image string) (*state, error) {
	ctx := context.Background()

	newClient, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, err
	}

	config := &container.Config{
		Image: image,
		Tty:   false,
	}

	cwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	hostConfig := &container.HostConfig{
		Binds: []string{fmt.Sprintf("%s:%s", cwd, "/var/project")},
	}

	con, err := newClient.ContainerCreate(ctx, config, hostConfig, nil, nil, name)
	if err != nil {
		return nil, err
	}

	return newState(con.ID), nil
}
