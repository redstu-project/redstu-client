package docker

import (
	"context"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
)

func RunContainer(name, image string) (string, error) {
	ctx := context.Background()

	newClient, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return "", err
	}

	newState, err := CreateContainer(name, image)
	if err != nil {
		return "", err
	}

	err = newClient.ContainerStart(ctx, newState.Container.ID, types.ContainerStartOptions{})
	if err != nil {
		return "", err
	}

	return "", nil
}
