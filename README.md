# Redstu Client

## Contents

## Features

- Create a mock project
- Install Redstu engine docker image

## Usage

### Generate a sample project

```bash
redstu init sample-project

Generating mock project
File: .redstu
File: src/main.js
File: redstu.yml
File: .redstu/states
File: .redstu/states/default.json
File: .redstu/signals
File: .redstu/signals/default.json
File: .gitignore
File: package.json

Successully created a redstu mock project!Execute the following commands in order to get started
cd sample-project
```

### Install Redstu Engine docker image

```bash
redstu engine install
```

### Examples

## Prerequisites

- [Git](https://git-scm.com)
- [Docker](https://docker.io)

## Installation

## Configuration
### Generate a main configuration file
```bash
redstu setup
```

### Generate an alternative configuration file
```bash
redstu setup --profile dev
```
## Related Projects
- [Redstu Engine](https://gitlab.com/redstu-project/redstu-engine)

## Contributing

## Changelog

## Authors

- [Edson Michaque](https://gitlab.com/edsonmichaque)

## Credits

## License