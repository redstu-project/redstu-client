.PHONY: test build clean
build: build-linux build-macos build-windows

release: release-linux release-macos release-windows


.PHONY: package
package:  package-linux package-windows package-macos

.PHONY: dependencies
dependencies:
	@go get golang.org/x/lint/golint

.PHONY: lint
lint: dependencies
	@${GOPATH}/bin/golint ./...

test:
	@go test -v ./...

clean:
	rm -rf bin
	rm -rf dist
	rm -rf coverage

race:
	@go test -race -v ./...


msan:
	@CC=clang go test -msan -v ./...


coverage:
	@bash scripts/coverage.sh

reports:
	@bash scripts/coverage.sh html

build-linux: cmd/redstu/main.go
	@bash scripts/build.sh linux

build-macos: cmd/redstu/main.go
	@bash scripts/build.sh macos

build-windows: cmd/redstu/main.go
	@bash scripts/build.sh windows

package-linux: bin/linux/amd64/redstu
	@bash scripts/dist.sh linux
	
package-macos: bin/macos/amd64/redstu
	@bash scripts/dist.sh macos

package-windows: bin/windows/amd64/redstu.exe
	@bash scripts/dist.sh windows
	
package: package-linux package-macos package-windows

install-linux: bin/linux/amd64/redstu
	@install bin/linux/amd64/redstu /usr/local/bin/

release-linux: cmd/redstu/main.go
	@bash scripts/release.sh linux

release-macos: cmd/redstu/main.go
	@bash scripts/release.sh macos

release-windows: cmd/redstu/main.go
	@bash scripts/release.sh windows
